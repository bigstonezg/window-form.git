package com.dlcs.window;

import com.dlcs.window.form.*;

public class Main {
    public static void main(String[] args) {
        WinForm wf = new WinForm();
        wf.add(new Picture());
        wf.add(new Button("登录"));
        wf.add(new Button("注册"));
        Frame frame = new Frame("Frame1");
        frame.add(new Label("用户名"));
        frame.add(new TextBox("文本框"));
        frame.add(new Label("密码"));
        frame.add(new PasswordBox("密码框"));
        frame.add(new CheckBox("复选框"));
        frame.add(new TextBox("记住用户名"));
        frame.add(new LinkLabel("忘记密码"));
        
        wf.add(frame);
        
        wf.print();
    }
}
