package com.dlcs.window.form;

import java.util.Vector;

public class Frame extends AbstractForm {
    private Vector<IForm> vector = new Vector<IForm>();
    public Frame(String _label){
        super(_label);
    }
    @Override
    public void print() {
        super.print();
        for (IForm iform:
             vector) {
            iform.print();
        }
    }
    
    public void add (IForm form){
        vector.add(form);
    }
}
