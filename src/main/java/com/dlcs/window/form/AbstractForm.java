package com.dlcs.window.form;

public abstract class AbstractForm implements IForm {
    private String label;
    public AbstractForm(String _label){
        this.label = _label;
    }
    
    public void print() {
        System.out.println("print " + this.getClass().getSimpleName() + "(" + getLabel() + ")");
    }
    
    public String getLabel(){
        return this.label;
    }
}
